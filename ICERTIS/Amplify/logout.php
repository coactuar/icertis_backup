<?php
require_once "inc/config.php";
require_once "functions.php";
$userid = $_SESSION['userid'];
$emailid= $_SESSION['emailid'];
$member = new User();
$member->__set('user_id', $userid);
$status = $member->userLogout();

$ad_logout_url="https://login.microsoftonline.com/0d7a41cc-1a62-4347-bacd-764a455b5cf2/oauth2/v2.0/logout?post_logout_redirect_uri=https://coact.live/icertis";
 
  $post_data = "";

  $parts = explode("@",$emailid); 
  $domain = $parts[1]; 


  if($domain=='icertis.com'){
  
      // Prepare new cURL resource
      header("location:".$ad_logout_url);
      unset($_SESSION['userid']);
      unset($_SESSION['emailid']);
      exit;
  }

//var_dump($status);
//echo $status['status'];

if ($status['status'] == 'success') {
  unset($_SESSION['userid']);
  unset($_SESSION['emailid']);
  // header("location: feedback.php");
  header("location: index.php");
}


