//cats and dogs
var img1 = {
  ansCoords: [  // right answer LOCUS
    [321, 389],  // [x, y] zeroed coordinates
    [558, 430],  
    [19, 280],  
    [260, 238],
    [310, 250]
    
  ],
  ansArea: [  // right answer AREA
    [30, 20], // [x-radius, y-radius]
    [20, 20], 
    [70, 50], 
    [30, 20],
    [40, 40]
 
  ],
  cssClass: 'img1',
  found: [] // will receive arrays of found areas from logFound()
};

//cats and dogs pink background
var img4 = {
  ansCoords: [  // right answer LOCUS
    [90, 320],  // [x, y] zeroed coordinates
    [250, 480],  
    [385, 260],  
    [450, 475],
    [630, 410]
    
  ],
  ansArea: [  // right answer AREA
    [50, 50], // [x-radius, y-radius]
    [50, 50], 
    [40, 40], 
    [30, 20],
    [30, 30]
 
  ],
  cssClass: 'img4',
  found: [] // will receive arrays of found areas from logFound()
};
