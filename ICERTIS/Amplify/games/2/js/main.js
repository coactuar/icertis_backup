jQuery(document).ready(function($){
	$('.wheel-standard').superWheel({
		slices: [
		{
			text: "20% OFF",
			value: 1,
			message: "You win 20% off",
			discount: "Coact20",
			background: "grey",
			
		},
		{
			text: "Unlucky luck",
			value: 0,
			message: "You have No luck Sorry",
			discount: "No",
			background: "red",
			
		},
		{
			text: "30% OFF",
			value: 1,
			message: "You win 30% off",
			discount: "Coact30",
			background: "#627506",
			
		},
		{
			text: "Lose",
			value: 0,
			message: "You Lose :(",
			discount: "No",
			background: "#E74C3C",
			
		},
		{
			text: "40% OFF",
			value: 1,
			message: "You win 40% off",
			discount: "Coact40",
			background: "blue",
			
		},
		{
			text: "Nothing",
			value: 0,
			message: "You get Nothing :(",
			discount: "No",
			background: "#550A29",
			
		}
	],
	text : {
		color: '#fff',
	},
	line: {
		width: 10,
		color: "#ecf0f1"
	},
	outer: {
		width: 14,
		color: "#ecf0f1"
	},
	inner: {
		width: 15,
		color: "#ecf0f1"
	},
	marker: {
		background: "#e53935",
		animate: 1
	},
	
	selector: "value",

	});

	var tick = new Audio('audio/alien.mp3');
	
	$(document).on('click','.wheel-standard-spin-button',function(e){
		
		$('.wheel-standard').superWheel('start','value',Math.floor(Math.random() * 2));
		$(this).prop('disabled',true);
	});

	$('.wheel-standard').superWheel('onStep',function(results){
		
		if (typeof tick.currentTime !== 'undefined')
			tick.currentTime = 0;
        
		 tick.play();
		
	});
	
	$('.wheel-standard').superWheel('onStart',function(results){
		
		
		$('.wheel-standard-spin-button').text('Spinning...');
		
	});
	

	$('.wheel-standard').superWheel('onComplete',function(results){
		//console.log(results.value);
		if(results.value === 1){
			
			swal({
				type: 'success',
				title: "Congratulations!", 
				html: results.message+' <br><br><b>Discount : [ '+ results.discount+ ' ]</b>'
			});
			
		}else{
			swal("Oops!", results.message, "error");
		}
		
		
		$('.wheel-standard-spin-button:disabled').prop('disabled',false).text('Spin');
		
	});
});